package com.myduka.app.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.myduka.app.R;

import java.util.List;


/**
 * Created by Brayo on 9/27/2016.
 */
public class SlideshowAdapter extends RecyclerView.Adapter<SlideshowAdapter.SlideshowViewHolder> {

    private List<String> showcase;
    private int rowLayout;
    private Context context;

    public static class SlideshowViewHolder extends RecyclerView.ViewHolder {
        LinearLayout categoryLayout;
        ImageView slideshow_image;

        public SlideshowViewHolder(View v) {
            super(v);
            slideshow_image = (ImageView) v.findViewById(R.id.slideshow_image);
        }
    }

    public SlideshowAdapter(List<String> showcase, int rowLayout, Context context) {
        this.showcase = showcase;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public SlideshowViewHolder onCreateViewHolder(ViewGroup parent,
                                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new SlideshowViewHolder(view);
    }


    @Override
    public void onBindViewHolder(SlideshowViewHolder holder, final int position) {

        switch (position){
            case 0:
                holder.slideshow_image.setImageDrawable(context.getResources().getDrawable(R.drawable.fruits_a));
                break;
            case 1:
                holder.slideshow_image.setImageDrawable(context.getResources().getDrawable(R.drawable.fruits_b));
                break;
            case 2:
                holder.slideshow_image.setImageDrawable(context.getResources().getDrawable(R.drawable.fruits_c));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return showcase.size();
    }
}