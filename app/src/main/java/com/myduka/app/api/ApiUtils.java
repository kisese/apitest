package com.myduka.app.api;

import com.myduka.app.api.services.STKPushService;

/**
 * Created by USER on 5/28/2017.
 */

public class ApiUtils {
    public static final String BASE_URL = "https://sandbox.safaricom.co.ke/";

    public static STKPushService getTasksService(String token) {
        return RetrofitClient.getClient(BASE_URL, token).create(STKPushService.class);
    }
}
